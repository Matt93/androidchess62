package view;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import model.Piece;
import model.Square;

public class SquareAdapter extends BaseAdapter {

	private Context context;
	private Square[][] board;

	public SquareAdapter(Context c, Square[][] board) {
		this.context = c;
		this.board = board;
	}

	public int getCount() {
		return 64;
	}

	public Object getItem(int position) {
		return null;
	}

	public long getItemId(int position) {
		return 0;
	}


	public View getView(int position, View convertView, ViewGroup parent) {

		ImageView imageView;
		if (convertView == null) {

			imageView = new ImageView(context);
			imageView.setLayoutParams(new GridView.LayoutParams(60, 60));
			imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
			imageView.setPadding(5, 5, 5, 5);

			int col = position / 8 % 2;
			if (col == 0) {
				if (position % 2 == 0)
					imageView.setBackgroundColor(Color.rgb(169, 169, 169));
				else
					imageView.setBackgroundColor(Color.WHITE);
			} else {
				if (position % 2 == 0)
					imageView.setBackgroundColor(Color.WHITE);
				else
					imageView.setBackgroundColor(Color.rgb(169, 169, 169));
			}

			Piece p = board[position / 8][position % 8].piece;

			if( p != null)
				imageView.setImageResource(context.getResources().getIdentifier(p.toString(), "drawable", context.getPackageName()));
		} else {
			imageView = (ImageView) convertView;
		}


		return imageView;
	}
}


